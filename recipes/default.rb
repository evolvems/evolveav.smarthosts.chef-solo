#
# Cookbook Name:: evolve_deploy
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'git'
include_recipe "apache2"
include_recipe "php"
include_recipe "php::module_mysql"
include_recipe "php::module_curl"
include_recipe "apache2::mod_php5"
include_recipe "python"
include_recipe "python::pip"


apache_site "default" do
  enable false
end

template "/home/ubuntu/.ssh/bitbucket-read.pem" do
	source "bitbucket-key.erb"
	mode 0400
	owner 'ubuntu'
	group 'ubuntu'
end

template "/home/ubuntu/.ssh/config" do
	source "ssh_config.erb"
	mode 0755
	owner 'ubuntu'
	group 'ubuntu'
end

virtual_hosts = data_bag("evolve_sites")
virtual_hosts.each do | host |

	opts = data_bag_item("evolve_sites", host)

	vhost_def opts['id'] do
		gitroot opts["gitroot"]
		public_dir opts["public_dir"]
		git_repo opts['git_repo']
		hostname opts['hostname']
	end

end #vhost.each

#Lets Set Up The Hook
directory "/var/hook/" do
	action :create
	owner 'ubuntu'
	group 'www-data'
	mode 0775
	recursive true
end

v_hosts = Array.new
virtual_hosts.each do | host |
	opts = data_bag_item("evolve_sites", host)
	v_hosts.push(opts["gitroot"])
end

#Get the AWS SDK
remote_file "/var/hook/aws.phar" do
	source 'https://github.com/aws/aws-sdk-php/releases/download/2.6.15/aws.phar'
end

#Lets Set up th Git UpdateNanny
directory "/home/ubuntu/git" do
	action :create
	owner 'ubuntu'
	group 'www-data'
	mode 0775
	recursive true
end

template "/home/ubuntu/git/gitUpdator" do
	source "gitUpdater.erb"
	mode 0775
	owner 'ubuntu'
	group 'www-data'
	variables({
		:vhosts => v_hosts
	})
end

template "/home/ubuntu/git/updater-nanny" do
	source "updater-nanny.erb"
	mode 0775
	owner 'ubuntu'
	group 'www-data'
end

template "/etc/cron.d/updater-nanny" do
	source "crontab.erb"
	mode 0644
	owner 'root'
	group 'root'
end


template "/var/hook/snsEndpoint.php" do
	source "snsEndpoint.php.erb"
	mode 0755
	owner "www-data"
	group "www-data"
	variables({
		:vhosts => v_hosts
	})
end

web_app "hook" do
	template "hook.conf.erb"
end #end of web_app

python_pip "awscli"

execute "RegisterNode" do
	command "aws sns subscribe --topic-arn arn:aws:sns:us-west-2:911178247648:Smarthosts --protocol http --notification-endpoint http://$(ec2metadata --public-hostname):1080/snsEndpoint.php --region us-west-2"
end

execute "Updater-Nanny" do
	command "/home/ubuntu/git/updater-nanny"
	user 'ubuntu'
end