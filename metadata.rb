name             'evolve_deploy'
maintainer       'Evolve Audio Visual'
maintainer_email 'service.desk@evolveav.co.za'
license          'All rights reserved'
description      'Installs/Configures Evovle Audio Visual Smart Hosts'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.1'

depends "apache2"
depends "php"
depends "python"
depends "sudo"