define 	:vhost_def,
		:gitroot => '',
		:public_dir => '',
		:git_repo => '',
		:template => 'vhost.conf.erb',
		:hostname => '' do



#Lets get the src from GIT
gitroot = params[:gitroot]
git_rep = params[:git_repo]

#Lets Create the Directory for GIT
directory gitroot do
	action :create
	owner 'ubuntu'
	group 'www-data'
	mode 0755
	recursive true
end

git gitroot do
	repository git_rep
  	revision "master"
  	action :sync
  	user 'ubuntu'
end #end of git

#Lets Define the Variables for the Params To Pass to new Definition
docroot = params[:gitroot] + "/" + params[:public_dir]
template = params[:template]
hostname = params[:hostname]
webapp_id = params[:name]

web_app webapp_id do
	template template
	docroot docroot
	server_name hostname
end #end of web_app


end #End of Define